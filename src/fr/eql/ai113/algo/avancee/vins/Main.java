package fr.eql.ai113.algo.avancee.vins;

import java.io.IOException;
import java.io.RandomAccessFile;

public class Main {
    private static final int tailleMaximaleLigne = 100;
    private static final String FILE_IN = "VINStp.DON";
    private static final String FILE_OUT = "vins_tries.txt";

    /**
     * Cette méthode permet de recopier le contenu du fichier VINStp.DON dans un nouveau fichier, en faisant en sorte
     * que chaque ligne soit un tableau de caractères de 100 éléments.
     * @param inputFilepath Emplacement du fichier source.
     * @throws IOException Si la lecture ou l'écriture d'un raf ne fonctionne pas (fichier inexistant par exemple).
     */
    private static void structurer(String inputFilepath) throws IOException {

        // Ici, on crée quelque chose qui nous permet d'accéder à un fichier en lecture (à l'adresse 'inputFilepath')
        RandomAccessFile raf = new RandomAccessFile(inputFilepath, "r");
        // Nom du fichier choisi pour la sortie
        String result = FILE_OUT;
        // Ouverture d'une autorisation d'écriture sur le fichier dont on a choisi le nom
        RandomAccessFile raf2 = new RandomAccessFile(result, "rw");


        String line = null;
        // On va parcourir le fichier source ligne par ligne, jusqu'à ce que readLine() renvoie null (ça veut dire que
        // le fichier est terminé) puis recopier chaque ligne dans une ligne de 'tailleMaximaleLigne'.
        while ((line = raf.readLine()) != null) {
            // On transforme chaque ligne source en 'tableau de caractères'
            char[] L = line.toCharArray();
            // On crée une nouvelle ligne dans laquelle on va recopier
            char[] O = new char[tailleMaximaleLigne];
            // On recopie
            for (int i = 0 ; i < L.length ; i++) {
                O[i] = L[i];
            }
            // On comble la fin de chaque ligne créée par des espaces, pour ne pas avoir des 'null'
            for (int i = L.length ; i < O.length ; i++) {
                O[i] = ' ';
            }
            // On ajoute des sauts de ligne à la fin de chaque ligne (sinon tout serait mis à la suite)
            O[tailleMaximaleLigne - 2] = '\r';
            O[tailleMaximaleLigne - 1] = '\n';
            // Si on fait writeBytes, c'est parce que sinon java écrit sur 2 octets : 1 null et 1 caractère.
            // Ici, cela permet d'écrire uniquement le caractère.
            raf2.writeBytes(new String(O));
        }
    }

    /**
     * Cette méthode permet de trier le fichier qu'on a créé avec la méthode 'structurer'.
     * @param filePath Emplacement (sur le disque dur) du fichier qu'on veut trier.
     * @throws IOException Si la lecture ou l'écriture d'un raf ne fonctionne pas (fichier inexistant par exemple).
     */
    private static void trierFichier(String filePath) throws IOException {
        // On crée le raf qui permet de lire et d'écrire sur le fichier
        RandomAccessFile raf = new RandomAccessFile(filePath, "rw");
        // On détermine le nombre de lignes 'n' en divisant la taille totale du contenu du fichier (raf.length() )
        // par le nombre d'éléments d'un tableau de caractères représentant une ligne ('n' doit être un long, car un raf.length() est un long).
        long n = raf.length() / tailleMaximaleLigne;
        // Ici on crée un nouveau tableau de byte de 100 éléments, toujours à cause du souci de char qui fait
        // 2 bytes dans java, et 1 byte dans un fichier texte. On s'assure ainsi que les calculs que fera le raf seront corrects.
        byte[] Lt = new byte[tailleMaximaleLigne];
        // On parcourt le fichier ligne par ligne 'n' fois (nombre de lignes).
        for (int i = 0 ; i < n - 1 ; i++) {
            byte[] Li = recupererLigne(raf, i);
            // On va comparer la ligne en cours avec chaque ligne qui la suit ('i' + 1 jusqu'à 'n')
            for (int j = i + 1 ; j < n ; j++) {
                byte[] Lj = recupererLigne(raf, j);
                // Si la ligne en cours ('i') est plus "grande" que la ligne qu'on compare ('j'), alors on échange les deux
                // afin que la ligne qui arrive avant dans l'ordre alphabétique se retrouve en haut.
                if (comparerLigne(Li, Lj) > 0) {
                    // Séquence pour échanger les 2 lignes grâce à un cycle de 3 échanges avec une ligne tampon.
                    deverser(Li, Lt);
                    deverser(Lj, Li);
                    deverser(Lt, Lj);

                    // On réécrit les lignes avec les contenus échangés juste avant.
                    raf.seek((long) i * tailleMaximaleLigne);
                    raf.write(Li);
                    raf.seek((long) j * tailleMaximaleLigne);
                    raf.write(Lj);
                }
            }
        }
    }

    /**
     * Permet de copier le contenu d'une ligne du fichier texte dans un tableau de bytes dans la RAM, pour pouvoir
     * la traiter ensuite.
     * @param raf raf qui permet d'accéder au fichier qu'on traite.
     * @param i Numéro de la ligne à laquelle on se trouve dans le fichier.
     * @return Renvoie la ligne qu'on a copiée en mémoire vive.
     * @throws IOException Si la lecture ou l'écriture d'un raf ne fonctionne pas (fichier inexistant par exemple).
     */
    private static byte[] recupererLigne(RandomAccessFile raf, int i) throws IOException {
        // On se place à la bonne ligne.
        raf.seek((long) tailleMaximaleLigne * i);
        // On crée un tableau de bytes en RAM.
        byte[] result = new byte[tailleMaximaleLigne];
        // On remplit le tableau avec le contenu de la ligne 'i'.
        raf.read(result);

        return result;
    }

    /**
     * Permet de comparer 2 Strings : si le résultat est < 0, le premier String arrive avant dans l'ordre
     * alphabétique. Si c'est > 0, c'est qu'il arrive après, et si c'est == 0, c'est que les 2 Strings comparés
     * sont identiques.
     * @param A Premier String à comparer
     * @param B Deuxième String à comparer
     * @return Le résultat de l'opération (un int qui permettra de savoir selon la règle au-dessus quel String arrive
     * avant dans l'ordre alphabétique).
     */
    private static int comparerLigne(byte[] A, byte[] B) {
        String a = new String(A);
        String b = new String(B);
        // Méthode qui permet d'obtenir un int, qui détermine quelle phrase est plus 'grand' que l'autre.
        return a.compareTo(b);
    }

    /**
     * Permet de copier tout le contenut d'un tableau de bytes dans un autre tableau de bytes.
     * @param A Tableau source.
     * @param B Tableau cible.
     */
    private static void deverser(byte[] A, byte[] B) {
        // On parcourt le tableau A en entier
        for (int i = 0 ; i < A.length ; i++) {
            // Pour chaque élément du tableau A, on le copie dans le tableau B.
            B[i] = A[i];
        }
    }

    /**
     * Méthode qui permet de rechercher une ligne dans notre fichier (donc le nom d'un vin, si le fichier est
     * correctement rempli).
     * @param filepath Emplacement du fichier source sur le disque dur.
     * @param value Quelle ligne on recherche (la recherche s'effectuera uniquement par le début de ligne).
     * @return La ligne recherchée : si un début de ligne correspond exactement, renvoie la ligne en question.
     * <p>Si aucune ligne ne correspond exactement, renverra la ligne la plus proche.
     * @throws IOException Si la lecture ou l'écriture d'un raf ne fonctionne pas (fichier inexistant par exemple).
     */
    public static String search (String filepath, String value) throws IOException {
        RandomAccessFile raf = new RandomAccessFile(filepath, "r");
        byte[] result = chercherRec(value.getBytes(), 0, (int) raf.length() / tailleMaximaleLigne, raf);
        return new String(result);
    }


    /**
     * Méthode qui effectue réellement la recherche qu'on souhaite au sein d'un fichier.
     * <p>Méthode récursive (qui s'appelle elle-même), qu'on a aussi appelée "accumulateur" en cours, qui permet
     * une recherche dichotomique au sein du fichier (dichotomique = on se place au milieu, si le résultat qu'on
     * recherche est plus petit, on élimine toute la moitié supérieure. Puis on se place au milieu de ce qui reste,
     * et on recommence <== c'est là qu'intervient la récursivité).
     * @param V Ce qu'on recherche.
     * @param i Ligne de début de recherche.
     * @param j Ligne de fin de recherche.
     * @param raf raf qui permet d'accéder au fichier qu'on traite.
     * @return La ligne recherchée, ou la plus proche (juste au-dessus dans l'ordre alphabétique) si inexistante.
     * @throws IOException Si la lecture ou l'écriture d'un raf ne fonctionne pas (fichier inexistant par exemple).
     */
    private static byte[] chercherRec(byte[] V, int i, int j, RandomAccessFile raf) throws IOException {
        // Tant qu'il y a au moins une ligne entre 'i' et 'j' (les bornes de début et fin de recherche).
        if (j - i > 1) {
            // On se place au milieu, puis on recupère et compare avec notre recherche.
            int milieuIndex = (i + j) / 2;
            byte[] milieuLigne = recupererLigne(raf, milieuIndex);
            // Si notre recherche est avant dans l'alphabet, alors on recherche de 'i' jusqu'à la ligne du milieu.
            if (comparerLigne(V, milieuLigne) < 0) {
                // C'est ici qu'a lieu la récursivité, car on rappelle la méthode dans laquelle on se trouve
                // Mais en changeant les bornes de recherche. On part de 'i', jusqu'au milieu de la section d'avant.
                return chercherRec(V, i, milieuIndex, raf);
            }
            else {
            // Si notre recherche est après dans l'alphabet, on recherche de la ligne du milieu jusqu'à la fin de la section.
                // Pareil, récursivité, mais on démarre au milieu de la section d'avant, jusqu'à la borne 'j'.
                return chercherRec(V, milieuIndex, j, raf);
            }
        }
        // S'il ne reste plus de ligne entre 'i' et 'j' après la recherche dichotomique, on arrête la récursivité.
        else {
            // On récupère alors les deux lignes sur lesquelles on se trouve.
            byte[] Li = recupererLigne(raf, i);
            byte[] Lj = recupererLigne(raf, j);

            // On compare celle du dessus avec notre recherche, et si elle correspond exactement, on la renvoie.
            // Ici, ne pas oublier que 'return' permet également de terminer l'exécution de la méthode.
            if (comparerLigne(V, Li) == 0) {
                return Li;
            }
            // Si la ligne du dessus ne correspondait pas exactement, on teste avec la ligne du dessous, et on retourne si c'est égal.
            else if (comparerLigne(V, Lj) == 0) {
                return Lj;
            }
            // Si aucune des deux lignes ne correspondait, alors on choisit (arbitrairement) de renvoyer la ligne du dessous.
            else {
                return Lj;
            }
        }
    }




    public static void main(String[] args) {

        // On crée le fichier de recherche.
        try {
            structurer(FILE_IN);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // On trie le fichier de recherche.
        try {
            trierFichier(FILE_OUT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // On recherche ce qu'on souhaite dans le fichier trié, et on l'affiche en console.
        try {
            String resultatRecherche = search(FILE_OUT, "Marquis De Saint");
            System.out.println("Résultat le plus proche sinon exact : " +
                    "\r\n" + resultatRecherche);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }
}
